<?php require "bdd/bddconfig.php";
session_start();

$paramOK = false;
// Recup les 3 variables POST et les sécurise
if ((isset($_POST['acces'])) && (isset($_POST['titre'])) && (isset($_POST['texte'])) && (isset($_POST['idTheme'])) && (isset($_SESSION['logged_in']['idUser']))) {
    $acces = htmlspecialchars($_POST['acces']);
    $titre = htmlspecialchars($_POST['titre']);
    $texte = htmlspecialchars($_POST['texte']);
    $idTheme = htmlspecialchars($_POST['idTheme']);
    $idUser = htmlspecialchars($_SESSION['logged_in']['idUser']);
    $paramOK = true;
}?>

<h1>TEST</h1>

<?php 


// INSERT dans la base
if ($paramOK == true) {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $addArticle = $objBdd->prepare("INSERT INTO article (titre, texte,acces,idTheme,idUser) VALUES (:titre, :texte, :acces, :idTheme,:idUser)");
    $addArticle->bindParam(':titre', $titre, PDO::PARAM_STR);
    $addArticle->bindParam(':texte', $texte, PDO::PARAM_STR);
    $addArticle->bindParam(':acces', $acces, PDO::PARAM_STR);
    $addArticle->bindParam(':idTheme', $idTheme, PDO::PARAM_INT);
    $addArticle->bindParam(':idUser', $idUser, PDO::PARAM_INT);
    $addArticle->execute();

    $lastId = $objBdd -> lastInsertId();
    echo $lastId;
}

$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
$page = 'index.php';
header("Location: http://$serveur$chemin/$page");
?>