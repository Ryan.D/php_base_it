<?php ob_start(); ?>
<?php session_start(); ?>

<?php
//Requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
$objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$lastArticle = $objBdd->query("SELECT * FROM article ORDER BY datePub DESC LIMIT 5");


while ($article = $lastArticle->fetch()) {
?>
    <h3> <?php echo $article['titre'] ?> </h3>
    <p><?php echo $article['datePub'] ?></p>
<?php
} //fin du while
$lastArticle->closeCursor(); //libère les ressources de la bdd
?>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>