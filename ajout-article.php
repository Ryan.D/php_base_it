<?php $titre = "test" ?>
<?php ob_start(); ?>
<?php session_start(); ?>

<?php
//Requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
$objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$nomTheme = $objBdd->query("SELECT * FROM theme");
$nomDoc = $objBdd->query("SELECT * FROM document");
?>

<form action="insert-article.php" method="POST">
    <div>
        <select name="idTheme" id="idTheme">
            <?php

            while ($temp1 = $nomTheme->fetch()) {
            ?>
                <option value="<?php echo $temp1['idTheme'] ?>"> <?php echo $temp1['nom'] ?> </option>
            <?php
            } //fin du while
            $nomTheme->closeCursor(); //libère les ressources de la bdd
            ?>
        </select>
        <select name="acces" id="acces">
            <option value="private">Private</option>
            <option value="public">Public</option>
        </select>
    </div>
    <input type="text" name="titre" id="titre" placeholder="Titre">
    <textarea name="texte" id="texte"></textarea>
    <input type="text" name="url" id="url" placeholder="Entrer votre URL">
    <input type="submit" value="Valider">
</form>



<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>