<?php ob_start(); ?>
<?php session_start(); ?>

<div class="listeArticle">
    <h2>Se connecter</h2>
</div>

<div class="formulaire">

    <form method="POST" action="login_action.php">
        <input type="text" name="login" placeholder="Saisissez votre login..." required>
        <input type="password" name="password" placeholder="Mot de passe" required>
        <input type="submit" value="Se connecter">
    </form>
</div>



<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>