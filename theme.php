<?php $titre = "test" ?>
<?php ob_start(); ?>
<?php session_start(); ?>

<?php
$idTheme = 0;
//Tester si les variables POST existent
$paramOK = false;
if (isset($_GET["idTheme"])) {
    $idTheme = intval(htmlspecialchars($_GET['idTheme']));
}
//Requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
$objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$contenuTheme = $objBdd->prepare("SELECT * FROM article,user WHERE article.idUser = user.idUser AND idTheme = :id");
$contenuTheme->bindParam(':id', $idTheme, PDO::PARAM_INT);
$contenuTheme->execute();

$lastArticle = $objBdd->prepare("SELECT * FROM theme WHERE idTheme = :id");
$lastArticle->bindParam(':id', $idTheme, PDO::PARAM_INT);
$lastArticle->execute();

?>

<?php
while ($test = $lastArticle->fetch()) {
?>
    <div class="listeArticle">
        <h3><?php echo $test['nom'] ?></h3>
    </div>

<?php
} //fin du while
$lastArticle->closeCursor(); //libère les ressources de la bdd

?>

<?php
while ($article = $contenuTheme->fetch()) {
?>
    <div class="listeArticle">
        <ul>
            <li><a href="article.php?idArticle=<?php echo $article['idArticle'] ?>">
                    <?php echo $article['titre'] ?> - Auteur : <?php echo $article['pseudo'] ?> - <?php echo $article['datePub'] ?></a></li>
        </ul>

    </div>

<?php
} //fin du while
$contenuTheme->closeCursor(); //libère les ressources de la bdd
?>
<?php
if (isset($_SESSION['logged_in']['login']) != "") {
    //l'internaute est authentifié
    //affichage "Se déconnecter"(logout.php), "Prif", "Paramètres", etc ...
?>
    <div class="addArt"><a href="ajout-article.php">Ajout d'un article</a></div>
<?php }
?>



<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>