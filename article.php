<?php ob_start(); ?>
<?php session_start(); ?>

<?php
$idArticle = 0;
//Tester si les variables POST existent
if (isset($_GET["idArticle"])) {
    $idArticle = intval(htmlspecialchars($_GET['idArticle']));
}
//Requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
$objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$contenuArticle = $objBdd->prepare("SELECT * FROM article,user WHERE article.idUser = user.idUser AND idArticle = :id");
$contenuArticle->bindParam(':id', $idArticle, PDO::PARAM_INT);
$contenuArticle->execute();
$result = $contenuArticle->fetch(PDO::FETCH_ASSOC);

$listeLien = $objBdd->prepare("SELECT * FROM document WHERE idArticle = :id AND type = 'lien'");
$listeLien->bindParam(':id', $idArticle, PDO::PARAM_INT);
$listeLien->execute();

?>

<div class="article">
    <div class="delUp">
        <h4> <?php echo $result['titre'] ?> </h4>
        <a href="deleteArticle.php?idArticle=<?php echo $idArticle; ?>">Delete</a>
        <form action="update.php" method="POST">
            <input type="hidden" name="idArticle" value="<?php echo $idArticle ?>">
            <input type="hidden" name="titre" value="<?php echo $result['titre'] ?>">
            <input type="hidden" name="texte" value="<?php echo $result['texte'] ?>">
            <input type="submit" value="Update">
        </form>

    </div>

    <p>Auteur : <?php echo $result['pseudo'] ?> - <?php echo $result['datePub'] ?></p>
    <p><?php echo $result['texte'] ?></p>
    <h2>Liens associés</h2>
    <?php

    if ($listeLien->rowCount() > 0) {
        while ($lien = $listeLien->fetch()) {
    ?>

            <div class="listeArticle">
                <a href=<?php echo $lien['url'] ?> target="_blank"> <?php echo $lien['nom'] ?> </a>
            </div>

    <?php
        } //fin du while
        $listeLien->closeCursor(); //libère les ressources de la bdd
    }
    ?>

    <input type="hidden" name="idArticle" value="<?php echo $idArticle ?>">
    <input type="hidden" name="titre" value="<?php echo $result['titre'] ?>">
    <input type="hidden" name="texte" value="<?php echo $result['texte'] ?>">

</div>



<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>