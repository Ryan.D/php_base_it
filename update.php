<?php ob_start(); ?>
<?php session_start(); ?>

<?php $titre = htmlspecialchars(strval($_POST["titre"]));
$texte = htmlspecialchars(strval($_POST["texte"]));
$idArticle = htmlspecialchars($_POST["idArticle"]);

?>

<form action="updateArticle.php" method="POST">
    <input type="text" name="titre" id="titre" value="<?php echo $titre ?>">
    <textarea name="texte" id="textearea"><?php echo $texte ?></textarea>
    <input type="hidden" name="idArticle" value="<?php echo $idArticle ?>">
    <input type="submit" value="Valider">
</form>



<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>