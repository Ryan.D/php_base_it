<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php $titre = "Base de connaisance IT";
            echo $titre ?> </title>

    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div id="container">
        <div class="login">
            <div class="logo"><img src="https://upload.wikimedia.org/wikipedia/fr/d/dd/TOTAL_SA_logo.svg" alt="" height="50px"></div>
            <div class="titre">
                <h1>Base de connaisance IT</h1>
            </div>
            <div class="connexion">
            <?php
            if (isset($_SESSION['logged_in']['login']) != "") {
                //l'internaute est authentifié
                //affichage "Se déconnecter"(logout.php), "Prif", "Paramètres", etc ...
            ?>
                <a href="logout.php"> <?php echo $_SESSION['logged_in']['pseudo'] ?> - Deconnexion</a>
            <?php } else {
                //Personne n'est authentifié
                // affichage d'un lien pour se connecter
                // <a href="login.php">Connexion</a>

            ?>

                <a href="login.php">Connexion</a>
            <?php
            }
            ?>
            </div>
        </div>
        <div class="themeDoc">
            <nav class="theme">
                <h2>Théme</h2>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <?php
                    //Requete SQL
                    require "bdd/bddconfig.php";
                    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
                    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    $listeTheme = $objBdd->query("SELECT * FROM theme");


                    while ($theme = $listeTheme->fetch()) {
                    ?>
                        <li><a href="theme.php?idTheme=<?php echo $theme['idTheme']; ?>"><?php echo $theme['nom']; ?></a></li>
                    <?php
                    } //fin du while
                    $listeTheme->closeCursor(); //libère les ressources de la bdd
                    ?>
                </ul>
            </nav>
            <div class="doc">
                <?php echo $contenu; ?>
            </div>
        </div>
    </div>
</body>

</html>